package tarea2_semana2;

import java.util.Scanner;

public class TAREA2_SEMANA2 {

    public static void main(String[] args) {
        int opcion;
        Scanner sc = new Scanner(System.in);
        System.out.println("1: Edad");
        System.out.println("2: Salario");
        System.out.println("3: Numeros");
        System.out.println("4: Promedio");
        System.out.print("Opcion: ");
        opcion = sc.nextInt();
        switch (opcion) {
            case 1:
                Edades tp = new Edades();
                tp.edad();
                break;
            case 2:
                Salario tt = new Salario();
                tt.salariotrabajador();
                break;
            case 3:
                Numeros tc = new Numeros();
                tc.secuencia();
                break;
            case 4:
                promedio tv = new promedio();
                tv.promedios();
                break;
            default:
                System.out.printf("Opcion no valida");
                break;
        }

    }
}
